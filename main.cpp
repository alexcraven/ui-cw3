#include "tomeo.h"

int main(int argc, char *argv[]) {

    Tomeo *tom = new Tomeo(argc, argv);

    // showtime!
    tom->getWindow()->show();

    // wait for the app to terminate
    return tom->exec();
}
