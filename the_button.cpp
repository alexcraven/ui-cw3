//
// Created by twak on 11/11/2019.
//

#include "the_button.h"

#include <QFileInfo>
#include <QLabel>
#include <QVBoxLayout>

//initialises the (pointer to the) button (i think)
void TheButton::init(TheButtonInfo* i) {
    //the icon property is whats shown on the button so seticon just sets the buttons icon
    setIcon( *(i->icon) );
    info =  i;
    mContent = new QMediaContent(*this->info->url);

    //set the content of the QMediaPlayer to the video
    mPlayer->setMedia(*mContent);

    connect(this->mPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
            this,SLOT(getMetadata(QMediaPlayer::MediaStatus)));

    this->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    finfo->setFile(this->info->url->path());

}

//this function emits a signal "jumpTo" which jumps to the video
//that was clicked on (again, i think)
void TheButton::clicked() {
    emit jumpTo(info);
}

//gets filename & other properties of each video
void TheButton::getMetadata(QMediaPlayer::MediaStatus ms) {
    if (ms == QMediaPlayer::LoadedMedia){
        bDuration= qRound64( mPlayer->duration()/1000.0);
        date = mPlayer->metaData(QMediaMetaData::Date).toDate();
        location = mPlayer->metaData(QMediaMetaData::GPSAreaInformation).toString();
        QString filename=finfo->baseName();
        this->setText("Title: "+filename+"\nTime: "+QString::number(bDuration/3600)+":"+
                      QString::number((bDuration/60)%60)+":"+QString::number(bDuration%60));
        this->info->duration=bDuration;

    }
}

