/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */
#include "tomeo.h"
#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QScrollArea>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include "media_player.h"
#include <QObject>
#include <QComboBox>
#include <QFileInfo>
#include <QLineEdit>
#include <QMediaMetaData>
#include <QFont>
#include <QFileDialog>

using namespace std;

MediaPlayer *kPlayerButtons=0;
//constructor

Tomeo::Tomeo(int argc, char *argv[]) : QApplication(argc, argv) {
    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;
    videos = getInfoIn(string(argv[1]));
    if (videos.size() == 0) {
        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );
        switch( result ) {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }
    initialiseVariables();
    player->setVolume(1);player->setVideoOutput(videoWidget);
    search->setPlaceholderText("Search");
    connect(search,SIGNAL(textChanged(QString)),this,SLOT(searchVector(QString)));
    sort->setPlaceholderText("Sort by");
    sort->addItems((QStringList()<<"Name (A-Z)"<<"Name (Z-A)"<<"Duration (A)"<<"Duration (D)"));
    searchBar->addWidget(search); searchBar->addWidget(sort);
    connect(sort,SIGNAL(currentTextChanged(QString)),this,SLOT(sortVector(QString)));
    buttonWidget->setLayout(layout);layout->setContentsMargins(10, 10, 10, 10);
    spareButton->setVisible(false);rhsMenu->addLayout(searchBar);
    //creates the buttons to be added to the vector of buttons
    createButtons(&videos,&buttons);
    // tell the player what buttons and videos are available
    // and plays the first video
    player->setContent(&buttons, & videos);player->setPlaylist(playlist);
    playlist->setPlaybackMode(QMediaPlaylist::Sequential);
    connect(player,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,
            SLOT(updateTitle(QMediaPlayer::MediaStatus)));
    topWidgets();
    //connect MediaPlayer buttons
    kPlayerButtons->setIntState(player->state());
    leftLayout -> addLayout(currentTitle);
    leftLayout->addWidget(videoWidget);leftLayout->addWidget(kPlayerButtons);
    //formats the scroll area
    formatScroll(scroll);rhsMenu->addWidget(scroll);
    rightFrame = new QFrame();rightFrame->setLayout(rhsMenu);
    rightFrame->setFrameStyle(QFrame::Box);
    //create vertical box to add video player and rhsMenu to
    top->addLayout(leftLayout);top->addWidget(rightFrame);
    //sets colour of the window
    windowColour(window);
    window->setStyleSheet("QFrame {border: 2px solid #00ffb7} ");
    //adding everything to main window
    window->setLayout(top);
    window->setWindowTitle("tomeo");window->setMinimumSize(800, 680);
}

//destructor
Tomeo::~Tomeo() {}

void Tomeo::createButtons(vector<TheButtonInfo> *videos, vector<TheButton*> *buttons){
    // create the buttons
    for ( int i = 0; i < (int)videos->size(); i++ ) {
        //create thumbnail button which is a child of buttonWidget
        TheButton *button = new TheButton(buttonWidget);
        //depending on which thumbnail is pressed, the mediaplayer will play the corresponding vid
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player,
                        SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons->push_back(button);
        //adds the button to the QHBoxLayout
        button->setIconSize(QSize(200, buttonWidget->height()));
        layout->addWidget(button);
        //layout->addSpacing(5);

        //initialise the video ie assign the icon and info for the button
        button->init(&videos->at(i));
        button->setMaximumHeight(200);
        button->setStyleSheet("background-color:white;");
        playlist->addMedia(*button->mContent);
        connect(button->mPlayer, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
                button, SLOT(getMetadata(QMediaPlayer::MediaStatus)));
    }
}

void Tomeo::overwriteButtons(vector<TheButtonInfo> *videos, vector<TheButton*> *buttons)
{
    //delete children from buttonWidget
    while ( TheButton* w = buttonWidget->findChild<TheButton*>() )
        delete w;

    // create the buttons
    for ( int i = 0; i < (int)videos->size(); i++ ) {
        //create thumbnail button which is a child of buttonWidget
        TheButton *button = new TheButton(buttonWidget);
        //depending on which thumbnail is pressed, the mediaplayer will play the corresponding vid
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player,
                        SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons->at(i) = button;
        //adds the button to the QHBoxLayout
        button->setIconSize(QSize(200, buttonWidget->height()));
        layout->addWidget(button);

        //initialise the video ie assign the icon and info for the button
        button->init(&videos->at(i));
        button->setMaximumHeight(200);
        button->setStyleSheet("background-color:white;");
        playlist->addMedia(*button->mContent);
        connect(button->mPlayer, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
                button, SLOT(getMetadata(QMediaPlayer::MediaStatus)));
    }
}

void Tomeo::formatScroll(QScrollArea *scroll)
{
    scroll->setWidget(buttonWidget);
    scroll->setWidgetResizable(true);
    scroll->setMinimumWidth(250);
    scroll->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    scroll->setStyleSheet("border: none; background-color: #FFFFFF;");
}

void Tomeo::windowColour(QWidget *window)
{
    QPalette pal = palette();
    pal.setColor(QPalette::Background, QColor("#FFFFFF"));
    window->setPalette(pal);
}

void Tomeo::initialiseVariables()
{
    //widget that will show the video
    videoWidget = new QVideoWidget;
    //declaring QMediaPlaylist
    playlist = new QMediaPlaylist();
    // a column of buttons
    buttonWidget = new QWidget();
    //make a horizontal box for the search bar and filter icon
    searchBar = new QHBoxLayout();
    //make a search bar
    search = new QLineEdit();
    sort = new QComboBox();
    //make vbox to hold search bar and scroll area
    rhsMenu = new QVBoxLayout();
    // the buttons are arranged vertically
    layout = new QVBoxLayout();
    //Button used to dynamically retrieve the button height
    spareButton = new TheButton(buttonWidget);
    // create the main window and layout
    window = new QWidget;
    currentTitle = new QHBoxLayout();
    top = new QHBoxLayout();
    //add mediaPlayer buttons
    kPlayerButtons = new MediaPlayer(player, videoWidget->width(), this, playlist);
    leftLayout = new QVBoxLayout;// add the video & media controls to layout
    scroll = new QScrollArea();//Declaring scroll area
}

void Tomeo::topWidgets()
{
    QAbstractButton* addFile = new QToolButton();
    addFile->setIcon(QIcon(":/add.png"));
    addFile->setIconSize(QSize(35,35));
    connect(addFile, SIGNAL(clicked()), this, SLOT(showFile()));
    //adding a help button
    help = new QToolButton();
    help->setIcon(QIcon(":/help.png"));
    help->setIconSize(QSize(35,35));
    connect(help, SIGNAL(clicked()), this, SLOT(showHelp()));
//    vidTitle->setStyleSheet("border:none;");

    currentTitle->addWidget(addFile);
    currentTitle->addWidget(help);
    currentTitle->addWidget(vidTitle);
}

//returns QWidget ptr
QWidget* Tomeo::getWindow() {return window;}


//toggles fullscreen mode
void Tomeo::fScreenSlot() {
    QVBoxLayout *temp;
    if (!window->isFullScreen()) {
        //make fullscreen
        rhsMenu->removeWidget(scroll);
        rhsMenu->removeItem(searchBar);
        leftLayout->removeItem(currentTitle);
        top->removeItem(rhsMenu);
        top->removeWidget(rightFrame);
        videoWidget->raise();
        window->setWindowState(Qt::WindowFullScreen);
    }
    else {
        //return back to normal
        leftLayout->removeWidget(videoWidget);
        leftLayout->removeWidget(kPlayerButtons);
        rhsMenu->addLayout(searchBar);
        rhsMenu->addWidget(scroll);
        rightFrame->setLayout(rhsMenu);
        leftLayout->addLayout(currentTitle);
        leftLayout->addWidget(videoWidget);
        leftLayout->addWidget(kPlayerButtons);
        top->addLayout(leftLayout);
        top->addWidget(rightFrame);
        window->setWindowState(Qt::WindowNoState);
    }

    //reset
    window->update();
}


void Tomeo::updateTitle(QMediaPlayer::MediaStatus) {
    tempInfo->setFile(player->currentMedia().canonicalResource().url().path());
    vidTitle->setText(tempInfo->baseName());
    vidTitle->setAlignment(Qt::AlignHCenter);
    QFont f( "Arial", 20, QFont::Bold);
    vidTitle->setFont(f);
}

// read in videos and thumbnails to this directory
vector<TheButtonInfo> Tomeo::getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    //takes the path thats inputted from terminal as loc
    QDir dir(QString::fromStdString(loc) );
    //qdiriterator is used to navigate entries of a directory one at a time
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif
            //thumb is what it expects the name of the thumbnail file to be
            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail and returns image
                    //if sprite ISNT null
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}

void Tomeo::sortVector(QString selection){
    if (selection == "Name (A-Z)") { //sort A-Z
        std::sort(videos.begin(),videos.end(),
                  [](const TheButtonInfo& a,
                  const TheButtonInfo& b) { return a.filename < b.filename; });
    } else if(selection == "Name (Z-A)") { //sort Z-A
        std::sort(videos.begin(),videos.end(),
                  [](const TheButtonInfo& a,
                  const TheButtonInfo& b) { return a.filename > b.filename; });
    }else if(selection == "Duration (A)") { //sort duration Ascending
        std::sort(videos.begin(),videos.end(),
                  [](const TheButtonInfo& a,
                  const TheButtonInfo& b) { return a.duration < b.duration; });
    }else if(selection == "Duration (D)") { //sort duration Descending
        std::sort(videos.begin(),videos.end(),
                  [](const TheButtonInfo& a,
                  const TheButtonInfo& b) { return a.duration > b.duration; });
    }
    //sorting is done, now we need tomake a function which adds the videos to the scroll area
    //so we can refresh the window with the correctly sorted scroll area
    overwriteButtons(&videos,&buttons);
    window->update();
}

void Tomeo::searchVector(QString search){
    if(search == ""){
        //return full videos vector
        currentV=videos;
    } else {
        currentV.clear();
        for (int i=0; i<videos.size() ;i++ ) {
            QString temp = videos.at(i).filename;
            if (temp.contains(search, Qt::CaseInsensitive))
            {
                currentV.push_back(videos.at(i));
            }
        }
    }
    overwriteButtons(&currentV,&buttons);
    scroll->update();
    window->update();
}

void Tomeo::showHelp(){
    Help dialog;
    dialog.setModal(true);
    dialog.exec();
}

void Tomeo::showFile()
{
    QString fileName = QFileDialog::getOpenFileName(this->getWindow(), tr("Open File"),
                                                    "/home",
                                                    tr("Videos (*.wmv *.mp4 *MOV)"));
    qDebug() << "at the moment, this functionality hasn't yet been implemented, but when it "
                "is implemented, it will append the file to the scroll area widget!" <<endl;
}
