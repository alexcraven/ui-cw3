//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
//"buttons -> at(0) -> info" just basically gets the info (URL and icon) of
//the button at the 0th index in the array "buttons"
    jumpTo(buttons -> at(0) -> info);
}

//dont need shuffle

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    if (ms == QMediaPlayer::State::StoppedState) {
    }
}

//just plays the video
void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

