#ifndef MEDIA_PLAYER_H
#define MEDIA_PLAYER_H

#include <QWidget>
#include <QMediaPlayer>
#include <QAbstractButton>
#include <QComboBox>
#include <QLabel>
#include "help.h"
#include "tomeo.h"
#include <QHBoxLayout>

//defining members & methods of MediaPlayer class which is subclass of QWidget
class MediaPlayer : public QWidget {

    Q_OBJECT

public:
    MediaPlayer(ThePlayer *player, int vidWidth, Tomeo *tom, QMediaPlaylist *playlist);
    QMediaPlayer::State getState();
    qreal getSpeed(int index);
    int getVol();
    bool getMuted();
    void initialiseVariables();

private:
    //class variables
    ThePlayer *player = 0;
    QVideoWidget *videoWidget = 0;
    QMediaPlaylist *playlist = 0;
    bool muted = false;
    bool looped = false;
    int vidWidth = 0;
    Tomeo *tom = 0;
    QHBoxLayout *buttonsLayout;

    //media player buttons & sliders
    QMediaPlayer::State intState;
    QAbstractButton *playB;
    QComboBox *speedB;
    QSlider *sliderB;
    QSlider *volumeB;
    QAbstractButton *muteB;
    QAbstractButton *nextB;
    QAbstractButton *prevB;
    QLabel *timestamp;
    QAbstractButton *fScreenB;
    QAbstractButton *loopB;

    //internal function
    void connectStuff();
    void doButtonLayout();

public slots:
    void setIntState(QMediaPlayer::State state);
    void setVolume(int volume);
    void setMute(bool mute);

private slots:
    void playSlot();
    void speedSlot(int index);
    void setPos(int pos);
    void sliderSlot(qint64 pos);
    void newLength(qint64 len);
    void muteSlot();
    void prevSlot();
    void timeSlot(int seconds);
    void loopSlot();

signals:
    void play();
    void pause();
    void changeRate(qreal rate);
    void changeVolume(int volume);
    void changeMuting(bool muting);
    void next();
    void previous();

};

#endif // MEDIA_PLAYER_H
