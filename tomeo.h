#ifndef TOMEO_H
#define TOMEO_H

#include <QApplication>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QWidget>
#include "the_button.h"
#include "the_player.h"
#include <vector>
#include <QVBoxLayout>
#include <QScrollArea>

class Tomeo : public QApplication {

Q_OBJECT

public:
    Tomeo(int argc, char *argv[]);
    QWidget* getWindow();
    ~Tomeo();
    void createButtons(vector<TheButtonInfo> *videos,vector<TheButton*> *buttons);
    void overwriteButtons(vector<TheButtonInfo> *videos, vector<TheButton*> *buttons);
    void formatScroll(QScrollArea *scroll);
    void windowColour(QWidget *window);
    void initialiseVariables();
    void topWidgets();
    vector<TheButtonInfo> getInfoIn (string loc);

private:
    QWidget *window;
    QVBoxLayout *leftLayout;
    QHBoxLayout *top;
    QHBoxLayout *currentTitle;
    QVBoxLayout *rhsMenu;
    QScrollArea *scroll;
    QHBoxLayout *searchBar;
    QVideoWidget *videoWidget;
    QLabel* vidTitle = new QLabel();
    QFileInfo* tempInfo = new QFileInfo();
    ThePlayer *player = new ThePlayer;
    QMediaPlaylist* playlist;
    QWidget *buttonWidget;
    QLineEdit* search;
    QComboBox* sort;
    QVBoxLayout *layout;
    TheButton *spareButton;
    QAbstractButton *help;
    QFrame* rightFrame;

    // a list of the buttons
    vector<TheButton*> buttons;
    vector<TheButtonInfo> videos;
    vector<TheButtonInfo> currentV;

public slots:
    void fScreenSlot();
    void updateTitle(QMediaPlayer::MediaStatus);
    //function to the videos array
    void sortVector(QString select);
    void searchVector(QString search);
    void showHelp();
    void showFile();
};

#endif // TOMEO_H
