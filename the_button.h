//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H

#include <QMediaMetaData>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QPushButton>
#include <QToolButton>
#include <QUrl>
#include <QWidget>
#include <QObject>
#include <QDate>
#include <QFileInfo>



class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display
    QFileInfo* finfo;
    QString filename="empty";
    qint64 duration =1;


    TheButtonInfo ( QUrl* url, QIcon* icon) : url (url), icon (icon) {
        finfo = new QFileInfo(url->path());
        filename = finfo->baseName();
    }


};

class TheButton : public QToolButton {
    Q_OBJECT

public:
    TheButtonInfo* info;
    QFileInfo* finfo = new QFileInfo();

    QMediaPlayer *mPlayer = new QMediaPlayer();
    QMediaContent *mContent;
    qint64 bDuration ; //video duration
    QDate date; //upload date
    QString location; //gps location

    TheButton(QWidget *parent) :  QToolButton(parent) {
         setIconSize(QSize(200,200));
         connect(this, SIGNAL(released()), this, SLOT (clicked() ));
    }

    void init(TheButtonInfo* i);
public slots:
    void getMetadata(QMediaPlayer::MediaStatus ms);

private slots:
    void clicked();


signals:
    void jumpTo(TheButtonInfo*);
    void durationChanged(qint64 newValue);

};

#endif //CW2_THE_BUTTON_H
