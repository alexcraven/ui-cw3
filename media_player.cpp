#include "media_player.h"
#include <QBoxLayout>
#include <QToolButton>
#include <QStyle>
#include <QMessageBox>
#include <string>

//media player constructor
MediaPlayer::MediaPlayer(ThePlayer *player, int vidWidth, Tomeo *tom, QMediaPlaylist *playlist) :
                      playB(0), player(player), vidWidth(vidWidth), tom(tom), playlist(playlist),
                  videoWidget(videoWidget), muteB(0), volumeB(0), speedB(0), nextB(0), prevB(0) {
    //adding & connecting play button
    initialiseVariables();
    playB->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    connect(playB, SIGNAL(clicked()), this, SLOT(playSlot()));
    playB->setStyleSheet("background-color: #00ffb7");

    //adding & connecting next & prev buttons
    prevB->setIcon(QIcon(":/previous.png"));
    nextB->setIcon(QIcon(":/next.png"));
    connect(prevB, SIGNAL(clicked()), this, SLOT(prevSlot()));
    connect(nextB, SIGNAL(clicked()), this, SIGNAL(next()));
    prevB->setStyleSheet("background-color: #00ffb7");
    nextB->setStyleSheet("background-color: #00ffb7");

    //adding & connecting video speed selector
    speedB->addItem("0.25x", QVariant(0.25));speedB->addItem("0.5x", QVariant(0.5));
    speedB->addItem("1.0x", QVariant(1.0));speedB->addItem("1.25x", QVariant(1.25));
    speedB->addItem("1.5x", QVariant(1.5));speedB->addItem("2x", QVariant(2.0));
    speedB->setCurrentIndex(2);
    connect(speedB, SIGNAL(activated(int)), SLOT(speedSlot(int)));

    //adding & connecting mute button & volume slider
    muteB->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
    muteB->setStyleSheet("background-color: #00ffb7");
    connect(muteB, SIGNAL(clicked()), this, SLOT(muteSlot()));

    volumeB->setRange(0, 100);
    volumeB->setValue(1);
    volumeB->setMinimumWidth(vidWidth * 0.2);
    volumeB->setMaximumWidth(vidWidth * 0.3);
    connect(volumeB, SIGNAL(sliderMoved(int)), this, SIGNAL(changeVolume(int)));

    //adding & connecting fullscreen button
    fScreenB->setIcon(QIcon(":/fullscreen.png"));
    connect(fScreenB, SIGNAL(clicked()), tom, SLOT(fScreenSlot()));
    fScreenB->setStyleSheet("background-color: #00ffb7");

    //adding & connecting main video slider
    sliderB->setRange(0, (player->duration() / 1000));
    connect(sliderB, SIGNAL(sliderMoved(int)), this, SLOT(setPos(int)));

    //adding timestamp label to slider
    connect(sliderB, SIGNAL(valueChanged(int)), this, SLOT(timeSlot(int)));

    //adding a help button
    loopB->setIcon(QIcon(":/loop_inactive.png")); loopB->setIconSize(QSize(20,20));
    loopB->setStyleSheet("background-color: #ED1C24");
    connect(loopB, SIGNAL(clicked()), this, SLOT(loopSlot()));
    //adding all components to layout
    doButtonLayout();
    QHBoxLayout *sliderLayout = new QHBoxLayout;
    sliderLayout->addWidget(sliderB);sliderLayout->addWidget(timestamp);
    QVBoxLayout *mediaPlayerLayout = new QVBoxLayout;
    mediaPlayerLayout->addLayout(sliderLayout);mediaPlayerLayout->addLayout(buttonsLayout);
    setLayout(mediaPlayerLayout);
    //connects player to MediaPlayer buttons
    connectStuff();
}

//connects player to MediaPlayer buttons
void MediaPlayer::connectStuff() {
    connect(this, SIGNAL(play()), player, SLOT(play()));
    connect(this, SIGNAL(pause()), player, SLOT(pause()));
    connect(this, SIGNAL(changeRate(qreal)), player, SLOT(setPlaybackRate(qreal)));
    connect(player, SIGNAL(stateChanged(QMediaPlayer::State)),
                this, SLOT(setIntState(QMediaPlayer::State)));
    connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(sliderSlot(qint64)));
    connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(newLength(qint64)));
    connect(this, SIGNAL(changeVolume(int)), player, SLOT(setVolume(int)));
    connect(player, SIGNAL(volumeChanged(int)), this, SLOT(setVolume(int)));
    connect(this, SIGNAL(changeMuting(bool)), player, SLOT(setMuted(bool)));
    connect(player, SIGNAL(mutedChanged(bool)), this, SLOT(setMute(bool)));
    connect(this, SIGNAL(next()), playlist, SLOT(next()));
    connect(this, SIGNAL(previous()), playlist, SLOT(previous()));
}

//format button layout
void MediaPlayer::doButtonLayout() {
    //adding all components to layout
    buttonsLayout = new QHBoxLayout;
    buttonsLayout->setMargin(0);
    buttonsLayout->addWidget(playB);
    buttonsLayout->addWidget(prevB);
    buttonsLayout->addWidget(nextB);
    buttonsLayout->addWidget(loopB);
    buttonsLayout->addWidget(speedB);    
    buttonsLayout->addStretch(vidWidth * 0.4); 
    buttonsLayout->addWidget(muteB);
    buttonsLayout->addWidget(volumeB);
    buttonsLayout->addWidget(fScreenB);

}

//returns internal state
QMediaPlayer::State MediaPlayer::getState() {return intState;}

//changes playB button between pause & play icons depending on current state
void MediaPlayer::setIntState(QMediaPlayer::State state) {

    if (state != intState) {
        intState = state;

        if (state == QMediaPlayer::PlayingState)
            playB->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
        else
            playB->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    }
}

//pauses/plays video upon button click
void MediaPlayer::playSlot() {
    if (intState == QMediaPlayer::PlayingState)
        emit pause();
    else
        emit play();
}

//returns current video speed
qreal MediaPlayer::getSpeed(int index) {return speedB->itemData(index).toDouble();}

//changes speed upon speedB selection changing
void MediaPlayer::speedSlot(int index) {emit changeRate(getSpeed(index));}

//updates slider position as video plays
void MediaPlayer::sliderSlot(qint64 pos) {
    if (!sliderB->isSliderDown())
        sliderB->setValue(pos / 1000);
}

//set video position when sliderB moved
void MediaPlayer::setPos(int pos) {
    player->setPosition(pos * 1000);
}

//set slider range when video duration changes
void MediaPlayer::newLength(qint64 len) {
    sliderB->setMaximum(len / 1000);
}

//returns current volume slider value
int MediaPlayer::getVol() {
    if(volumeB)
        return volumeB->value();
    else
        return 0;
}

//set volume slider value if changed
void MediaPlayer::setVolume(int vol) {
    volumeB->setValue(vol);
}

//returns whether mute button yes/no
bool MediaPlayer::getMuted() {
    return muted;
}

void MediaPlayer::initialiseVariables()
{
    playB = new QToolButton(this);
    prevB = new QToolButton(this);
    nextB = new QToolButton(this);
    speedB = new QComboBox(this);
    muteB = new QToolButton(this);
    volumeB = new QSlider(Qt::Horizontal, this);
    fScreenB = new QToolButton(this);
    sliderB = new QSlider(Qt::Horizontal, this);
    timestamp = new QLabel("0s");
    loopB = new QToolButton(this);
}

int currVol = 0;

//sets mute button icon
void MediaPlayer::setMute(bool mute) {
    if (mute != muted) {
        muted = mute;

        if (muted) {
            muteB->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
            currVol = getVol();
            volumeB->setValue(0);
        }
        else {
            muteB->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
            volumeB->setValue(currVol);
        }
    }
}

//changes muting upon mute button clicked
void MediaPlayer::muteSlot() {emit changeMuting(!muted);}

//skips 10 seconds backwards
void MediaPlayer::prevSlot() {
    //video been playing for 5 seconds or more?
    if (player->position() >= 5000)
        player->setPosition(0);
    else
        emit previous();
}

//getting value of the label
void MediaPlayer::timeSlot(int seconds) {
    int hour = 0;
    int minute = 0;
    int secs = 0;
    QString ind;

    //if the video longer than an hour
    if(seconds > 3600) {
        secs = seconds % 60;
        minute = (seconds - secs) % 3600;
        hour = seconds / 3600;
        ind = QString::number(hour)+":"+QString::number(minute)+":"+QString::number(secs);
        timestamp->setText(QString(ind+"s").arg(secs));
    }
    //if the video longer than a minute
    else if (seconds > 60) {
        secs = seconds % 60;
        minute = seconds / 60;
        ind = QString::number(minute)+":"+QString::number(secs);
        timestamp->setText(QString(ind+"s").arg(secs));
    }
    else {
        ind = QString::number(seconds);
        timestamp->setText(QString(ind+"s"));
    }
}

//toggling video looping on/off
void MediaPlayer::loopSlot() {
    if (looped) {
        looped = false;
        loopB->setIcon(QIcon(":/loop_inactive.png"));
        loopB->setStyleSheet("background-color: #ED1C24");
        playlist->setPlaybackMode(QMediaPlaylist::Sequential);
    }
    else {
        looped = true;
        loopB->setIcon(QIcon(":/loop.png"));
        loopB->setStyleSheet("background-color: #00ffb7");
        playlist->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
    }
}
